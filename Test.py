#!/usr/bin/python
from os import listdir, getcwd, rename
from os.path import isdir, join


def test():
    # directorio principal
    dir_path = getcwd()
    # Cadena a Reemplazar
    str_search = 'prueba'

    # Obtenemos los archivos
    print('Obtenemos los archivos del directorio: '+str(dir_path))
    for filename in listdir(dir_path):
        # Obtenemos la ruta del archivo
        path_origin = join(dir_path, filename)

        # Remplazamos
        filename = filename.replace(str_search, '')

        # Obtenemos la nueva ruta
        path_dest = join(dir_path, filename)

        # Renombramos
        rename(path_origin, path_dest)


if __name__ == "__main__":
    test()
